# grub2-theme-space

This is a space themed grub2 theme.

It is inspired by the work by <https://github.com/vinceliuice/grub2-themes>
and has swapped out the background to be images.

Modifications:

- different background
- positioning of boot selection menu
- positioning of boot timer

Hubble images are fetched from here:

- <https://science.nasa.gov/image-detail/42916480792-cd4b5fcfdf-o/>

## Usage

1. Build the rpm with `go-task build`
1. Install the rpm with `sudo dnf install ./dist/*.rpm`

Once the theme is installed you need to tell grub to use it

1. set `GRUB_THEME` in `/etc/default/grub` to `/boot/grub2/themes/grub2-theme-space/theme-1080p.txt`
1. Update the grub boot config with `sudo grub2-mkconfig -o /boot/grub2/grub.cfg`

