Name:    grub2-theme-space
Version: %{_version}
Release: %{_release}%{?dist}
Summary: Grub2 Theme - Space

License: GLPv3
Source0: %{archive}-%{_version}.tar.gz
Source1: https://github.com/vinceliuice/grub2-themes/archive/refs/tags/2022-10-30.tar.gz
Source2: https://smd-cms.nasa.gov/wp-content/uploads/2023/07/42916480792-cd4b5fcfdf-o.jpg

Recommends: grub2-efi
Recommends: grub2-tools

BuildArch: noarch

%description
Spaced themed grub2 theme.

%prep
# setup seems to extract both sources
%setup -q -b 1

%install
mkdir -p %{buildroot}/boot/grub2/themes/%{name}
chmod 0755 %{buildroot}/boot/grub2/themes/%{name}

install -m 0644 -t %{buildroot}/boot/grub2/themes/%{name} \
    %{_sourcedir}/42916480792-cd4b5fcfdf-o.jpg \
    ../grub2-themes-2022-10-30/assets/assets-select/select-1080p/*.png \
    ../grub2-themes-2022-10-30/common/{*.png,*.pf2} \
    ../grub2-themes-2022-10-30/LICENSE \
    modifications/theme-1080p.txt

# info.png is specified explicitly in theme-1080p.txt
install -m 0644 -T ../grub2-themes-2022-10-30/assets/info-1080p.png \
    %{buildroot}/boot/grub2/themes/%{name}/info.png

mkdir -p %{buildroot}/boot/grub2/themes/%{name}/icons
chmod 0755 %{buildroot}/boot/grub2/themes/%{name}/icons

install -m 0644 -t %{buildroot}/boot/grub2/themes/%{name}/icons \
    ../grub2-themes-2022-10-30/assets/assets-white/icons-1080p/*.png

%files
/boot/grub2/themes/%{name}

%post

%postun

%changelog

* Tue Dec 05 2023 Paul Robertson 1.0.0-1
- initial rpm creation
